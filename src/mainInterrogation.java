import core.Document;
import indexation.Index;

import javax.print.Doc;
import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;

public class mainInterrogation {
    public static void main(String []args) throws IOException {

        //initialise l'index
        Index index  = Index.deserialize("cisi");
        String query = "What is the usual relevance of the content of articles to their titles??";

        //Modele boolean
        IRModel mod = new Boolean(index);
        mod.runModel(query);

        //Model Vectoriel Produit Cartesien avec ponderation TF
        WeightTF w = new WeightTF(index);
        IRModel modCart = new VectorielCartesien(index, w);
        System.out.println("Affichage de TF" +modCart.runModel(query)+ "\n");

        //Model Vectoriel Produit Cosinus avec ponderation TFIDF
        WeighterTFIDF wI = new WeighterTFIDF(index);
        IRModel modCos = new VectorielCosinus(index, wI);
        System.out.println("Affichage de TFIDF :" +modCos.runModel(query));



        Lecture l1 = new Lecture("1", index);
        ArrayList<String> liste = l1.liste();
        System.out.println(liste);


        ArrayList<Document> doc = new ArrayList<Document>();

        for(String id : modCart.runModel(query).keySet()){
            doc.add(index.getDoc(id));
        }

        Calcul c1 = new Calcul(doc, l1, 10);



    }


}
