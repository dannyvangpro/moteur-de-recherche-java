import javax.swing.*;

public class TestFenetre {
    public static void main(String [] args){

        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run() {
                FenetreSaisie fenetre = new FenetreSaisie();
                fenetre.setVisible(true);

                JLabel label = new JLabel("Moteur de recherche");
                label.add(label);
                label.setVisible(true);

            }
        });
        try{
            Thread.sleep(1000000);
        }catch (InterruptedException e){}

        System.exit(0);
    }
}
