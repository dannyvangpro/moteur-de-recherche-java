import indexation.*;
import indexation.TextRepresenter;
import java.util.*;

public abstract class IRModel {
    protected Index index;
    public IRModel(Index i){
        this.index=i;
    }


    public HashMap<String,Integer> getQueryProcessed (String query){
        TextRepresenter textRep = index.getTextRepresenter();
        HashMap<String,Integer> ret = textRep.getTextRepresentation(query);
        return ret;

    }

    public HashMap<String,Double> getDocScores (HashMap<String,Integer> hm){
        HashMap<String,Double> h= new HashMap<String,Double>();
        return h;
    }

    public LinkedHashMap<String,Double> getRanking(HashMap<String,Double> docsScores) {
        List<Map.Entry<String, Double>> entries = new ArrayList<Map.Entry<String, Double>>(docsScores.entrySet());
        Collections.sort(entries,new Comparator<Map.Entry<String, Double>>(){

            public int compare (Map.Entry<String, Double> a, Map.Entry<String,Double> b){
                return b.getValue().compareTo(a.getValue());
            }
        });
        LinkedHashMap<String, Double> ret = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : entries) {
            ret.put(entry.getKey(), entry.getValue());
        }
        return ret;
    }


    public LinkedHashMap<String,Double> runModel(String query){
        HashMap<String,Integer> queryProceseed = getQueryProcessed(query);
        HashMap<String,Double> docsScore = getDocScores(queryProceseed);
        return getRanking(docsScore);

    }


    private HashMap<String,Integer> score(String s){
        HashMap<String,Integer> liste= new HashMap<String,Integer>();
        HashMap<String,Integer> hm = index.getTfsForStem(s);
        for(String e : hm.keySet()){
            int iInt=1;
            liste.put(e,iInt);
        }

        return liste;

    }

}
