import indexation.*;
public class MainIndexation {

    public static void main (String[] args){
        Index index = new Index("Manish", new ParserCISI(), new Stemmer());

        //Générer un index pour la collection de documents CISI
        index.index("cisi.txt");

        //convertir en un tableau d'octets que l'on peut ensuite écrire dans un fichier
        index.serialize("cisi");


    }
}
