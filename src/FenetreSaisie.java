import javax.swing.*;
import java.awt.*;


public class FenetreSaisie extends JFrame {
    private JTextField textField;
    private JLabel label;

    public FenetreSaisie(){
        super();
        build();
    }

    private void build(){
        setTitle("MTech Search");
        setSize(320, 240);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    private JPanel buildContentPanel(){
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        JLabel label = new JLabel("Moteur de recherche");
        panel.add(label);

        JTextField textField = new JTextField();
        textField.setColumns(10);

        panel.add(textField);


        return panel;
    }

    public JTextField getTextField(){
        return textField;
    }

    public JLabel getLabel() {
        return label;
    }


}
