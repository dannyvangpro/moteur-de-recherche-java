import indexation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class VectorielCosinus extends IRModel{
    private Weighter w;

    public VectorielCosinus(Index i, Weighter w) {
        super(i);
        this.w=w;
    }

    /*public Double CalculNorme(HashMap<String, Integer> entry) {
        return null;
    }*/

    public HashMap<String, Double> getDocScores(HashMap<String, Integer> queryProcessed) {
        HashMap<String, Double> docScores = new HashMap<String, Double>();
        VectorielCartesien tmp = new VectorielCartesien(index, (WeightTF) w);
        HashMap<String, Double> scoreTmp = tmp.getDocScores(queryProcessed);
        HashMap<String, Double> vecReq = new HashMap<String, Double>();
        vecReq = w.prequete(queryProcessed);
        HashMap<String, Double> vecDoc = new HashMap<String, Double>();
        double normeReq=0.;
        double normeDoc=0.;

        for (Map.Entry<String, Double> mot : vecReq.entrySet()) {
            normeReq += Math.pow(mot.getValue(),2);
        }
        for (String doc : index.getDocs().keySet()) {
            vecDoc = w.pdoc(doc);
            for (Entry<String, Double> mot : vecDoc.entrySet()) {
                normeDoc += Math.pow(mot.getValue(), 2);
            }
            if((scoreTmp.get(doc) / (normeReq*normeDoc)<=1 )){
                docScores.put(doc, scoreTmp.get(doc) / (normeReq*normeDoc));
            }

            normeDoc = 0.;
        }
        return docScores;
    }


}