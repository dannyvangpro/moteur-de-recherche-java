import indexation.Index;
import java.util.HashMap;
import java.util.Map.Entry;

public class WeighterTFIDF extends WeightTF{

    public WeighterTFIDF(Index i){
        super(i);
    }

    public HashMap<String,Double> pdoc(String doc){
        //calcule de la frequence combinée avec la fréquence inverse
        WeightTF wtf = new WeightTF(i);
        HashMap<String,Double> hm = new HashMap<String,Double>();
        int nb_doc=i.getDocs().size();

        for (Entry<String,Double> val_TF : wtf.pdoc(doc).entrySet()) {
            //ajout dans le HashMaps + application de la formule
            hm.put(val_TF.getKey(), val_TF.getValue()*Math.log(nb_doc/i.getTfsForStem(val_TF.getKey()).size()));
        }
        return hm;
    }

}

