import core.Document;
import indexation.Index;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/*public class TestIndexation {
    public static void main(String[] args) throws FileNotFoundException, IOException {

        Index i = Index.deserialize("cisi");

        System.out.println("Texte du documents 55 \n" +i.getDoc("55").getText());

        System.out.println("Les occurences du documents 55 sont : \n" + i.getTfsForDoc("55"));

        System.out.println("Les documents qui contiennent le mot attemps: \n" + i.getTfsForStem("attempt"));

        System.out.println("Le nombre de termes du documents: \n" +i.getTfsForDoc("55").size());

        System.out.println("Le nombre de termes contentant le mot attempt: \n" +i.getTfsForStem("55").size());

        //score aléatoire
        HashMap<String,Double> score = new HashMap<String, Double>();
        for(String doc: i.getDocs().keySet()){
            score.put(doc,(Math.round(Math.random()*10)+10.0));
        }

        System.out.println("Le score est de : \n " +score +"\n");


    }*/
public class TestIndexation {
    public static void main(String[] args){

        Index p = Index.deserialize("cisi");

        //Affichage du document 55
        try {

            System.out.println("affichage du doc 55: ");
            String s = p.getStrDoc("55");
            System.out.println(s);

        }catch (IOException e){}


        //affichage des mots inclus dans le doc 55
        HashMap<String,Integer> hm = p.getTfsForDoc("55");
        System.out.println("Affichage des mots inclus dans le doc 55\n" +hm+ "\n");

        //affichage des documents contenant attempt
        System.out.println("affichage des documents contenant attempt\n");
        HashMap<String,Integer> hm1 = p.getTfsForStem("attempt");
        System.out.println(hm1);
        System.out.println();

        System.out.println("affichage getDocs\n");
        System.out.println(p.getDocs());

        System.out.println("affichage getStems\n");
        System.out.println(p.getStems());

        System.out.println("affichage getDocs.size()" + p.getDocs().size());

/*
        int i=0;
        for(String t : hm.keySet()){
            i++;
            System.out.println("key: " + t + " value: " + hm.get(t));
        }

        //affichage le nombre de documents qui contiennent un terme donné
        System.out.println();
        System.out.println("i: "+ i);

        //affichage des texte d'un terme donné


*/
        System.out.println();
        try {

            Document doc = p.getDoc("1");
            System.out.println(p.getTfsForDoc("1"));
        }catch (IOException e) {}



        System.out.println();
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entrer un mot ");
        String str = sc.nextLine();
        for(String t : hm.keySet()){
            HashMap<String,Integer> hm2 = p.getTfsForStem(str);
            if(hm2.size()!=0){
                for(String c : hm2.keySet()){
                    try{
                        String s= Integer.toString(hm2.get(c));
                        System.out.println(p.getStrDoc(c));
                    }catch(IOException e){}
                }
            }
        }

        //Generation de HashMap + score aleatoire

        ArrayList<HashMap<String,Double>> score = new ArrayList<HashMap<String, Double>>();

        for(String t : p.getStems().keySet()){
            HashMap<String,Double> tmp = new HashMap<String,Double>();
            tmp.put(t,Math.random()*9999);
            score.add(tmp);
        }

    }

}
