import core.Document;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class Calcul {
    private ArrayList<Document> doc = new ArrayList<Document>();
    private Lecture l;

    public Calcul(ArrayList<Document> doc, Lecture l, int n){
        //constructeur
        this.l= l;
        int i=0; //compteur
        for(Document e: doc){
            if(i<n)
                this.doc.add(e);
            i++;
        }
    }

   public double precision(){
        return intersection()/abs(doc.size()*1.0);
    }

    public double rappel(){
        return intersection()/abs(l.texte().size()*1.0);
    }

    private double intersection(){
        //|Dn∩DocAttendus|
        double cpt=0.0;
        for(Document d1 : doc){
            for(Document d2 : l.texte()){
                if(d1.getId() == d2.getId())
                    cpt++;
            }
        }
        return abs( (double) cpt);
    }

    public double F_mesure() {
        double prec = this.precision();
        double rap = this.rappel();
        return 2.0 * (prec * rap) / (prec + rap);
    }

    public ArrayList<Document> getDoc(){ return doc;}

}
