import core.Document;
import indexation.Index;

import javax.print.Doc;
import java.io.*;
import java.util.ArrayList;


public class Lecture {
    private String fichier;
    private Index i;

    public Lecture(String s, Index i){
        this.fichier=s;
        this.i=i;
    }

    public ArrayList<String> liste(){

        ArrayList<String> liste = new ArrayList<String>();
        try {
            File file = new File(fichier);    //creates a new file instance
            FileReader fr = new FileReader("cisi.rel");   //reads the file
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            StringBuffer sb = new StringBuffer();    //constructs a string buffer with no characters
            String line;
            String tmp = "";
            while ((line = br.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {

                    if (line.charAt(i) != ' ' && line.charAt(i) != '\n' && line.charAt(i) != '.' && i < 13) {
                        tmp += line.charAt(i);
                    } else {
                        if (tmp != "") liste.add(tmp);
                        tmp = "";
                    }
                }
            }
            fr.close();    //closes the stream and release the resources
            System.out.println("Contents of File: ");
            System.out.println(sb.toString());   //returns a string that textually represents the object
        } catch (IOException e) {
            e.printStackTrace();
        }
        return liste;
    }

    public ArrayList<Document> texte(){
        //retourne la liste des Documents
        ArrayList<Document> liste = new ArrayList<Document>();

        //ouverture de fichier + récupération des données
        try {
            File file = new File("cisi.qry");    //creates a new file instance
            FileReader fr = new FileReader(file);   //reads the file
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            StringBuffer sb = new StringBuffer();    //constructs a string buffer with no characters
            String line;
            String tmp = "";
            int cpt=0;
            while ((line = br.readLine()) != null)
            {
                if((line.charAt(0)=='.') &&(line.charAt(1)=='I')){ //check si commmence par .I
                    tmp=String.valueOf(line.charAt(3));    //récupere le num
                    liste.add(i.getDoc(tmp)); //récupere le doc via le num
                }

            }
            fr.close();    //closes the stream and release the resources
            System.out.println("Contents of File: ");
            System.out.println(sb.toString());   //returns a string that textually represents the object
        } catch (IOException e) {
            e.printStackTrace();
        }
        return liste;
    }

}
