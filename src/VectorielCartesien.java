import indexation.*;
import java.util.HashMap;
import java.util.Map;

public class VectorielCartesien extends IRModel {
    private WeightTF w;

    public VectorielCartesien(Index i,WeightTF w){
        super(i);
        this.w = w;
    }
    public HashMap<String, Double> getDocScores(HashMap<String, Integer> queryProcessed) {
        //calcule du produit cartésien

        HashMap<String, Double> docScores = new HashMap<String, Double>();
        HashMap<String, Double> vecDoc = new HashMap<String, Double>();
        HashMap<String, Double> vecReq = new HashMap<String, Double>();
        vecReq = w.prequete(queryProcessed);
        double score=0.;

        for (String doc : index.getDocs().keySet()) {
            vecDoc = w.pdoc(doc);
            for (Map.Entry<String, Double> entry : vecReq.entrySet()) {
                if (vecDoc.containsKey(entry.getKey()))
                    score += entry.getValue()*vecDoc.get(entry.getKey());
            }
            docScores.put(doc, score);
            score=0;
        }

        return docScores;
    }

}
