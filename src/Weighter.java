import indexation.Index;

import java.util.HashMap;

public abstract class Weighter {
    protected Index i;

    public Weighter(Index i){
        this.i=i;
    }

    public abstract HashMap<String,Double> pdoc(String doc);
    public abstract HashMap<String,Double> prequete(HashMap<String,Integer> hm);
}
