import indexation.*;
import java.util.Map.Entry;

import java.util.HashMap;

public class WeightTF extends Weighter {

    public WeightTF(Index i) {
        super(i);
    }

    public HashMap<String, Double> pdoc(String doc) {
        //création du poids du document
        HashMap<String, Double> poids = new HashMap<String, Double>();
        int total = nbmotfordoc(i, doc);
        for (Entry<String, Integer> mot : i.getTfsForDoc(doc).entrySet()) {
            this.ajouter(poids, mot.getKey(), (double) mot.getValue() / total);
        }
        return poids;
    }

    public HashMap<String, Double> prequete(HashMap<String, Integer> dico) {
        //création de la requete
        HashMap<String, Double> req = new HashMap<String, Double>();
        int total = nbmotforrequete(dico);
        for (Entry<String, Integer> mot : dico.entrySet()) {
            ajouter(req,mot.getKey(),(double)mot.getValue()/total);
        }
        return req;
    }


    private void ajouter(HashMap<String, Double> hm, String keys, Double value) {
        hm.put(keys, value);
    }

    private int nbmotfordoc(Index i, String doc) {
        //calcule le nombre total de mots.
        Integer total = 0;
        for (Entry<String, Integer> liste : i.getTfsForDoc(doc).entrySet()) {
            total += liste.getValue();
        }
        return total.intValue();
    }

    private int nbmotforrequete(HashMap<String,Integer> hm){
        Integer total=0;
        for (Entry<String, Integer> entry : hm.entrySet()) {
            total += entry.getValue();
        }
        return total.intValue();
    }


}